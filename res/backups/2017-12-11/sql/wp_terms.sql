SET NAMES 'utf8';
DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (1,'Uncategorized','uncategorized',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (2,'Hollywood','hollywood',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (3,'Television','television',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (4,'Science, Technology, Engineering and Mathematics','stem',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (5,'Research','research',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (6,'Entertainment','entertainment',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (8,'Interests','interests',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (9,'DIY &amp; Tech Tips','diy-tech-tips',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (10,'Opinions','opinions',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (11,'code','code',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (12,'react-native','react-native',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (13,'nativescript','nativescript',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (14,'javascript','javascript',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (15,'post-format-aside','post-format-aside',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (16,'neflix','neflix',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (17,'streaming','streaming',0);
INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES (18,'netflix','netflix',0);
